function sortTable(n){
	var table = document.getElementById("table");
	var count = 0;
	var isSwitching = true;
	var data1, data2, i, rows, shouldSwitch;
	var direction = "ascending";

	while (isSwitching){
		isSwitching = false;
		rows = table.rows;
		for (i = 1; i<(rows.length-1); i++){
			shouldSwitch = false;
			data1 = rows[i].getElementsByTagName("TD")[n]
			data2 = rows[i+1].getElementsByTagName("TD")[n]

			if (direction == "ascending"){
				if (data1.innerHTML.toLowerCase() > data2.innerHTML.toLowerCase()){
					shouldSwitch = true;
					break;
				}
			}
			else if (direction == "descending"){
				if (data1.innerHTML.toLowerCase() < data2.innerHTML.toLowerCase()){
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch){
			rows[i].parentNode.insertBefore(rows[i+1], rows[i]);
			isSwitching = true;
			count = count + 1;
		} else{
			if (direction == "ascending" && count == 0){
				direction = "descending";
				isSwitching = true;
			}
		}
	}
}
