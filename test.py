import os
import sys
import unittest
import HtmlTestRunner
from create_db import db, Book, Author, Publisher

class DBTestCases(unittest.TestCase):
        def test_insert_book_1(self):
                s = Book(title="Some Book", author='J.K. Rowling', publisher='Some Publisher', ISBN='9781781100486', publicationDate='2015-12-08', description=None, image_url=None) #create new entry of book
                
                db.session.add(s) #adding to table
                db.session.commit() #commit to table

                r = db.session.query(Book).filter_by(title="Some Book").one() #getting entry
                self.assertEqual(str(r.title),"Some Book")
                self.assertEqual(str(r.author), "J.K. Rowling")
                self.assertEqual(str(r.publisher), "Some Publisher")
                self.assertEqual(str(r.ISBN), "9781781100486")
                self.assertEqual(str(r.publicationDate), "2015-12-08")

                db.session.query(Book).filter_by(title="Some Book").delete() #delete entry to NOT populate table
                db.session.commit() #commit changes
                
        def test_insert_book_2(self): #test nones
                s = Book(title="Some Book", author='J.K. Rowling', publisher='Some Publisher', ISBN='97817811004', publicationDate='2015-12-08', description=None) #create new entry of book
                
                db.session.add(s) #adding to table
                db.session.commit() #commit to table

                r = db.session.query(Book).filter_by(ISBN='97817811004').one() #getting entry by ISBN
                self.assertEqual(str(r.title), "Some Book")
                self.assertEqual(str(r.author), "J.K. Rowling")
                self.assertEqual(str(r.publisher), 'Some Publisher')
                self.assertEqual(str(r.ISBN), "97817811004")
                self.assertEqual(str(r.publicationDate), "2015-12-08")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.image_url), "None")

                db.session.query(Book).filter_by(title="Some Book").delete() #delete entry to NOT populate table
                db.session.commit() #commit changes
                
        def test_insert_book_3(self): #test two entries
                s = Book(title="Some Book", author='J.K. Rowling', publisher='Some Publisher', ISBN='9781781100486', publicationDate='2015-12-08', description=None) #create new entry of book
                
                db.session.add(s) #adding to table
                db.session.commit() #commit to table
                
                s = Book(title="Other Book", author='J.K. Rowling', publisher='Some Publisher', ISBN='9781781100487', publicationDate='2015-12-08', description=None) #create new entry of book

                db.session.add(s)
                db.session.commit()
                
                r = db.session.query(Book).filter_by(title="Other Book").one() #getting entry
                self.assertEqual(str(r.title), "Other Book")
                self.assertEqual(str(r.author), "J.K. Rowling")
                self.assertEqual(str(r.publisher), 'Some Publisher')
                self.assertEqual(str(r.ISBN), "9781781100487")
                self.assertEqual(str(r.publicationDate), "2015-12-08")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.image_url), "None")

                r = db.session.query(Book).filter_by(title="Some Book").one() #getting 2nd entry
                self.assertEqual(str(r.title), "Some Book")
                self.assertEqual(str(r.ISBN), "9781781100486")

                db.session.query(Book).filter_by(title="Some Book").delete() #delete entry to NOT populate table
                db.session.commit() #commit changes

                db.session.query(Book).filter_by(title="Other Book").delete() #delete entry to NOT populate table
                db.session.commit() #commit changes

        def test_insert_publisher_1(self):
                s = Publisher(name="Some Publisher", website="www.Some Publisher.com", description="something", location="somewhere", founded = "1980", wikipedia_url = "https://www.something.com", image_url = "image.jpg")
                        
                db.session.add(s)
                db.session.commit()

                r = db.session.query(Publisher).filter_by(name="Some Publisher").one()
                self.assertEqual(str(r.name), "Some Publisher")
                self.assertEqual(str(r.website), "www.Some Publisher.com")
                self.assertEqual(str(r.description), "something")
                self.assertEqual(str(r.location), "somewhere")
                self.assertEqual(str(r.founded), "1980")
                self.assertEqual(str(r.wikipedia_url), "https://www.something.com")
                self.assertEqual(str(r.image_url), "image.jpg")
                

                db.session.query(Publisher).filter_by(name="Some Publisher").delete()
                db.session.commit()

        def test_insert_publisher_2(self): # test nones
                s = Publisher(name="Some Publisher", website="www.Some Publisher.com")
                        
                db.session.add(s)
                db.session.commit()

                r = db.session.query(Publisher).filter_by(website="www.Some Publisher.com").one() #query by website
                self.assertEqual(str(r.name), "Some Publisher")
                self.assertEqual(str(r.website), "www.Some Publisher.com")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.location), "None")
                self.assertEqual(str(r.founded), "None")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")
                

                db.session.query(Publisher).filter_by(name="Some Publisher").delete()
                db.session.commit()

        def test_insert_publisher_3(self): #test two entries
                s = Publisher(name="Some Publisher", website="www.Some Publisher.com", description="something", location="somewhere", founded = "1980", wikipedia_url = "https://www.something.com", image_url = "image.jpg")
                        
                db.session.add(s)
                db.session.commit()

                s = Publisher(name="Other Publisher", website = "www.otherpublisher.com")

                db.session.add(s)
                db.session.commit()

                r = db.session.query(Publisher).filter_by(name="Some Publisher").one()
                self.assertEqual(str(r.name), "Some Publisher")
                self.assertEqual(str(r.website), "www.Some Publisher.com")
                self.assertEqual(str(r.description), "something")
                self.assertEqual(str(r.location), "somewhere")
                self.assertEqual(str(r.founded), "1980")
                self.assertEqual(str(r.wikipedia_url), "https://www.something.com")
                self.assertEqual(str(r.image_url), "image.jpg")

                r = db.session.query(Publisher).filter_by(name="Other Publisher").one()
                self.assertEqual(str(r.name), "Other Publisher")
                self.assertEqual(str(r.website), "www.otherpublisher.com")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.location), "None")
                self.assertEqual(str(r.founded), "None")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")
                

                db.session.query(Publisher).filter_by(name="Some Publisher").delete()
                db.session.commit()

                db.session.query(Publisher).filter_by(name="Other Publisher").delete()
                db.session.commit()

        def test_insert_author_1(self):
                s = Author(name="Some Author", born="July 31, 1965", description="something", almaMater="University of Exeter", nationality = "British", wikipedia_url=None, image_url=None)
                db.session.add(s)
                db.session.commit()

                r = db.session.query(Author).filter_by(name="Some Author").one()
                self.assertEqual(str(r.name), "Some Author")
                self.assertEqual(str(r.born), "July 31, 1965")
                self.assertEqual(str(r.description), "something")
                self.assertEqual(str(r.almaMater), "University of Exeter")
                self.assertEqual(str(r.nationality), "British")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")

                db.session.query(Author).filter_by(name="Some Author").delete()
                db.session.commit()

        def test_insert_author_2(self): # test all none
                s = Author(name="Some Author")
                db.session.add(s)
                db.session.commit()

                r = db.session.query(Author).filter_by(name="Some Author").one()
                self.assertEqual(str(r.name), "Some Author")
                self.assertEqual(str(r.born), "None")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.almaMater), "None")
                self.assertEqual(str(r.nationality), "None")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")

                db.session.query(Author).filter_by(name="Some Author").delete()
                db.session.commit()

        def test_insert_author_3(self):
                s = Author(name="Some Author", born="July 31, 1965", description="something", almaMater="University of Exeter", nationality = "British", wikipedia_url=None, image_url=None)
                db.session.add(s)
                db.session.commit()

                s = Author(name="Some Other Author", born="August 20th, 1980")
                db.session.add(s)
                db.session.commit()

                r = db.session.query(Author).filter_by(name="Some Author").one()
                self.assertEqual(str(r.name), "Some Author")
                self.assertEqual(str(r.born), "July 31, 1965")
                self.assertEqual(str(r.description), "something")
                self.assertEqual(str(r.almaMater), "University of Exeter")
                self.assertEqual(str(r.nationality), "British")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")

                r = db.session.query(Author).filter_by(name="Some Other Author").one()
                self.assertEqual(str(r.name), "Some Other Author")
                self.assertEqual(str(r.born), "August 20th, 1980")
                self.assertEqual(str(r.description), "None")
                self.assertEqual(str(r.almaMater), "None")
                self.assertEqual(str(r.nationality), "None")
                self.assertEqual(str(r.wikipedia_url), "None")
                self.assertEqual(str(r.image_url), "None")

                db.session.query(Author).filter_by(name="Some Author").delete()
                db.session.commit()

                db.session.query(Author).filter_by(name="Some Other Author").delete()
                db.session.commit()

def execute(): # Run tests through TestRunner
        loader = unittest.TestLoader()
        suite = unittest.TestSuite((loader.loadTestsFromTestCase(DBTestCases)))

        runner = HtmlTestRunner.HTMLTestRunner(output = "report")

        runner.run(suite)


