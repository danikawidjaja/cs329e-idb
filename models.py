from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgres://postgres:treehouse@db.ziecore.com/bookdb')
print(os.environ.get("DB_STRING", 'postgres://postgres:treehouse@db.ziecore.com/bookdb'));
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True #suppress warning message
app.config['TEMPLATES_AUTO_RELOAD'] = True

db = SQLAlchemy(app)

class Book(db.Model): 
	__tablename__ = 'books'
	title = db.Column(db.String(100), nullable=False, primary_key=True)
	author = db.Column(db.PickleType(), nullable=False)
	publisher = db.Column(db.PickleType(), nullable=False)
	ISBN = db.Column(db.String(100), nullable=True)
	publicationDate = db.Column(db.String(100), nullable=True)
	description = db.Column(db.String(5000), nullable=True)
	image_url = db.Column(db.String(100), nullable=True)

class Author(db.Model):
	__tablename__ = 'authors' 
	name = db.Column(db.String(100), nullable=False, primary_key=True)
	born = db.Column(db.String(100), nullable=True)
	almaMater = db.Column(db.String(1000), nullable=True)
	description = db.Column(db.String(5000), nullable=True)
	nationality = db.Column(db.String(80), nullable=True)
	image_url = db.Column(db.String(1000), nullable=True)
	wikipedia_url = db.Column(db.String(1000), nullable=True)


class Publisher(db.Model):
	__tablename__ = 'publishers'
	name = db.Column(db.String(100), nullable=False, primary_key=True)
	location = db.Column(db.String(100), nullable=True)
	founded = db.Column(db.String(80), nullable=True)
	website = db.Column(db.String(1000), nullable=True)
	description = db.Column(db.String(5000), nullable=True)
	wikipedia_url = db.Column(db.String(1000), nullable=True)
	image_url = db.Column(db.String(1000), nullable=True)

db.drop_all()
db.create_all() #create initial database
