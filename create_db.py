import json
from models import app, db, Book, Author, Publisher 

def load_json(filename):
	with open(filename) as file:
		jsonfile = json.load(file)
		file.close()
	return jsonfile
 
def setOfAttribute(list, attribute):
	result = set()
	for item in list:
		if attribute in item:
			result.add(item[attribute])
	return result

def create_books():
	books = load_json('books.json')
	print(len(books))
	for b in books:
		title = b['title']
		description = b.get('description')
		image_url = b.get('image_url')

		authors = b['authors']
		create_authors(authors)
		authors_names = setOfAttribute(authors, 'name')
		ISBN = b.get('isbn')
		publicationDate = b.get('publication_date')

		publishers = b['publishers']
		create_publishers(publishers)
		publishers_names = setOfAttribute(publishers, 'name')
		
		new_book = Book(title= title, ISBN = ISBN, publicationDate = publicationDate, author = authors_names, publisher = publishers_names, description = description, image_url = image_url)
		db.session.add(new_book)
		db.session.commit()

def create_authors(authors_data):
	for a in authors_data:
		check_name = a['name']		

		if (db.session.query(Author).filter_by(name=check_name).count() == 0 ):
			name = a['name']
			born = a.get('born')
			almaMater = a.get('alma_mater')
			description = a.get('description')
			nationality = a.get('nationality')
			image_url = a.get('image_url')
			wikipedia_url = a.get('wikipedia_url')
			new_author = Author(name=name, born=born, almaMater=almaMater, description=description, nationality=nationality, image_url=image_url, wikipedia_url=wikipedia_url)
			db.session.add(new_author)
			db.session.commit()

def create_publishers(publishers_data):
	for p in publishers_data:
		check_name = p['name']
		if (db.session.query(Publisher).filter_by(name=check_name).count() == 0 ):
			name = p['name']
			location = p.get('location')
			founded = p.get('founded')
			website = p.get('website')
			description = p.get('description')
			wikipedia_url = p.get('wikipedia_url')
			image_url = p.get('image_url')
	
			new_publisher = Publisher(name=name, location = location, description=description, website=website, founded = founded, wikipedia_url=wikipedia_url, image_url=image_url)
			db.session.add(new_publisher)
			db.session.commit()


create_books()