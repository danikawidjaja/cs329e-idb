from flask import render_template
#from models import  app, db, Book
from create_db import app, db, Book, create_books, Author, Publisher
from rename import rename


#books = [{'title': 'Software Engineering', 'id': '1'}, {'title':'Algorithm Design', 'id':'2'},{'title':'Python', 'id':'3'}]

@app.route('/')
def base():
    return render_template('base.html')

@app.route('/books/')
def books():
    books = db.session.query(Book).all()
    return render_template('books.html', books = books)
	

@app.route('/authors/')
def authors():
    authors = db.session.query(Author).all()
    return render_template('authors.html', authors = authors)

@app.route('/publishers/')
def publishers():
    publishers = db.session.query(Publisher).all()
    return render_template('publishers.html', publishers = publishers)

@app.route('/about/')
def about():
    return render_template('about.html')

@app.route('/books/<string:book_title>/')
def book(book_title):
    books = db.session.query(Book).filter_by(title = book_title).all()[0]
    
    description = books.description
    author = books.author
    return render_template('book.html', book = books, description = description, author = author)

@app.route('/authors/<string:name_author>/')
def author(name_author):
    authors = db.session.query(Author).filter_by(name = name_author).all()[0]

    description = authors.description
    born = authors.born
    almaMater = authors.almaMater
    nationality = authors.nationality
    return render_template('author.html', author = authors, description = description, born = born, almaMater = almaMater, nationality = nationality)

@app.route('/publishers/<string:name_publisher>/')
def publisher(name_publisher):
    publishers = db.session.query(Publisher).filter_by(name = name_publisher).all()[0]

    description = publishers.description
    location = publishers.location
    founded = publishers.founded
    website = publishers.website
    return render_template('publisher.html', publisher = publishers, description = description, location = location, founded = founded, website = website)

@app.route('/test')
def test():
    import test
    test.execute()
    rename()
    return render_template("report.html")

@app.route('/search/<string:prompt>')
def search(prompt):
    publishers = db.session.query(Publisher).filter(Publisher.name.ilike('%' + prompt + '%'))
    
    authors = db.session.query(Author).filter(Author.name.ilike('%' + prompt + '%'))

    books = db.session.query(Book).filter(Book.title.ilike('%' + prompt + '%'))
    return render_template('search.html', prompt = prompt, publishers = publishers, authors=authors, books= books)

if __name__ == "__main__":
    app.run()

